var canvas = document.getElementById("mycanvas");
var ctx = canvas.getContext('2d');
var undo = [];
var redo = [];
const undotime = 10;


var mode = "cursor";
var penwid = 10;
var typing = false;

var startpt = {x:0,y:0};
var startcvs = ctx.getImageData(0, 0, canvas.width, canvas.height);

var isPress = false;

function saveundo(){
  clearredo();
  
  var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  if(undo.length>=undotime){
    undo.shift();
    //console.log("undo.length="+undo.length);
  }
  undo.push(imgData);
  //console.log('save');
}

function getundo(){
  console.log("undo");
  if(undo.length>0){
    var imgData = undo.pop();
    saveredo();
    ctx.putImageData(imgData,0, 0);
    
    //console.log(undo.length);
  }
}

function saveredo(){
  var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  redo.push(imgData);
}

function getredo(){
  if(redo.length>0){
    var img = ctx.getImageData(0, 0, canvas.width, canvas.height);
    undo.push(img);
    var imgData = redo.pop();
    ctx.putImageData(imgData,0, 0);
    //console.log(undo.length);
    
  }
}

function clearredo(){
  redo=[];
}



function type(e){
    //console.log(1);
    var x = document.createElement("input");
    var fontsize = document.getElementById('font-size').value;
    var fontfamily = document.getElementById('font-family').value;
    x.type="text";
    x.id="type";
    //console.log(2);
    x.setAttribute("value", "");
    x.style.top = e.clientY + "px";
    x.style.left = e.clientX + "px";
    var tx = e.offsetX;
    var ty = e.offsetY;
    x.style.position = "absolute";
    x.style.background = "rgba(0,0,0,0)";
    x.style.border =  "none";
    x.style.height =  fontsize + "px";
    x.style.fontSize = fontsize + "px";
    x.style.fontFamily = fontfamily;
    x.style.cursor = "url('cursor/text.png'), auto";
    var clr = document.getElementById('color').value;
    x.style.color = clr;
    document.body.appendChild(x);
    x.focus();
    e.preventDefault();
    //console.log(3);
    x.addEventListener('focusout',()=>{
        var rect = canvas.getBoundingClientRect();
        ctx.font = fontsize+"px "+fontfamily;
        ctx.fillStyle = clr;
        //console.log(typeof(fontsize)+" "+typeof(ty));



        ctx.fillText(x.value, tx +1, ty+parseInt(fontsize)-1);//之後再調位置//parseint
        
        

        x.remove();//有可能出事?
    });
}



function penwidchange(){
    penwid = document.getElementById('penwid').value;
}

function getmousepos(e){
    var rect = canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
}

canvas.addEventListener('mousedown', function (e){
  if(mode=="pen"||mode=="eraser"){
      saveundo();//象鼻擦跟手線save
      isPress = true;
      old = {x: e.offsetX, y: e.offsetY};
      if(mode=="pen"){
        var clr=document.getElementById('color').value;
        ctx.strokeStyle = clr;
        ctx.fillStyle = clr;
      }
      else if(mode=="eraser"){
        ctx.globalCompositeOperation = 'destination-out';
      }

      ctx.beginPath();
      ctx.arc(e.offsetX, e.offsetY, penwid/2, 0, 2 * Math.PI);
    
      ctx.fill();
  }else if(mode=="text"){
      if(typing==false){
        saveundo();
        type(e);
        typing = true;
      }else{
        
        typing = false;
      }
  }else if(mode=="circle"||mode=="rect"||mode=="tri"){
    saveundo();
      isPress = true;
      startpt = {x: e.offsetX, y: e.offsetY};
      startcvs = ctx.getImageData(0, 0, canvas.width, canvas.height);
  }
});

canvas.addEventListener('mousemove', (e)=>{
  if (isPress) {
    var et = getmousepos(e);

    var x = et.x;
    var y = et.y;
    var clr=document.getElementById('color').value;
    if(mode=="pen"||mode=="eraser"){
        if(mode=="pen"){
            
            ctx.strokeStyle = clr;
            ctx.fillStyle = clr;
        }
        else if(mode=="eraser")ctx.globalCompositeOperation = 'destination-out';
    
        ctx.beginPath();
        ctx.arc(x, y, penwid/2, 0, 2 * Math.PI);
        
        ctx.fill();
    
        
        ctx.lineWidth = penwid;
        ctx.beginPath();
        ctx.moveTo(old.x, old.y);
        ctx.lineTo(x, y);
        
        ctx.stroke();
    
        old = {x: x, y: y};
    }else if(mode=="circle"||mode=="rect"||mode=="tri"){
        if(mode=="circle"){
          ctx.putImageData(startcvs,0, 0);
          ctx.strokeStyle = clr;
          ctx.fillStyle = clr;
          ctx.lineWidth = penwid;
          ctx.beginPath();
          ctx.arc((e.offsetX + startpt.x)/2, (e.offsetY + startpt.y)/2, Math.sqrt( Math.pow(startpt.x-e.offsetX,2) + Math.pow(startpt.y-e.offsetY,2) )/2, 0, 2 * Math.PI);
          ctx.stroke();
        }else if(mode=="rect"){
          ctx.putImageData(startcvs,0, 0);
          ctx.strokeStyle = clr;
          ctx.fillStyle = clr;
          ctx.lineWidth = penwid;
          ctx.beginPath();
          ctx.rect(startpt.x, startpt.y, e.offsetX-startpt.x,e.offsetY-startpt.y);
          ctx.stroke();
        }else if(mode=="tri"){
          ctx.putImageData(startcvs,0, 0);
          ctx.strokeStyle = clr;
          ctx.fillStyle = clr;
          ctx.lineWidth = penwid;
          ctx.beginPath();
          ctx.arc(e.offsetX,e.offsetY, penwid/2, 0, 2 * Math.PI);
          ctx.fill();
          ctx.beginPath();
          ctx.arc(startpt.x * 2- e.offsetX,e.offsetY, penwid/2, 0, 2 * Math.PI);
          ctx.fill();
          ctx.beginPath();
          ctx.arc(startpt.x,startpt.y, penwid/2, 0, 2 * Math.PI);
          ctx.fill();
          
          

          ctx.beginPath();
          
          ctx.moveTo(startpt.x,startpt.y);
          ctx.lineTo(e.offsetX,e.offsetY);
          
          ctx.moveTo(e.offsetX,e.offsetY);
          ctx.lineTo(startpt.x * 2- e.offsetX,e.offsetY);


          ctx.moveTo(startpt.x * 2- e.offsetX,e.offsetY);
          ctx.lineTo(startpt.x,startpt.y);
          ctx.stroke();

          
        }
    }
  }
});
canvas.addEventListener('mouseup', (e)=>{
  isPress = false;
  ctx.globalCompositeOperation= 'source-over';
});

function modechange(event){
    var e = event.target.id;
    if(e=="cursor"){
      mode="cursor";
      canvas.style.cursor = "default";
    }else if(e=="pen"){
        mode="pen";
        canvas.style.cursor = "url('cursor/pen.png'), auto";
    }else if(e=="eraser"){
        mode="eraser";
        canvas.style.cursor = "url('cursor/eraser.png'), auto";
    }else if(e=="text"){
        mode="text";
        canvas.style.cursor = "url('cursor/text.png'), auto";
    }else if(e=="circle"){
        mode="circle";
        canvas.style.cursor = "url('cursor/circle.png'), auto";
    }else if(e=="rect"){
      mode="rect";
      canvas.style.cursor = "url('cursor/rect.png'), auto";
    }else if(e=="tri"){
      mode="tri";
      canvas.style.cursor = "url('cursor/tri.png'), auto";
    }
    console.log(mode);
}

function refresh(){
    saveundo();//save
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}  
  
/*document.getElementById('reset').addEventListener('click', ()=>{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);*/

function loadimg(id){
  var img = document.createElement('input');
  img.addEventListener('change',readFile,false);
  img.type = 'file';
  img.accept = 'image/*';
  img.id = id;
  img.click();
}

function readFile(){
  var file = this.files[0];
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function(e){
    var img = new Image();
    img.src = this.result;

    img.onload = function(){
      saveundo();//圖片save
      ctx.drawImage(img,0,0);
    //strDataURI = cvs.toDataURL();//獲取canvas base64資料
    }
  }
}

function download(id){
  var img = document.createElement('a');
  img.href="#";
  img.download="下載.png";
  img.addEventListener('click',()=>{
    img.href=canvas.toDataURL();
  });
  img.id = id;
  img.click();
}




